from flask import Flask, request
from flask_cors import CORS
import time, os, shutil, glob, logging, socket,pprint
from flask import send_from_directory

app = Flask(__name__)
CORS(app)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.DEBUG,
    datefmt='%Y-%m-%d %H:%M:%S')

@app.route('/api',methods=('get', 'post','put','patch','delete'))
def api():
    logging.info(request.method)
    #pprint.pprint(request.method)
    logging.info("headers")
    #pprint.pprint("headers")
    pprint.pprint(request.headers)
    logging.info("parameters")
    #pprint.pprint("parameters")
    pprint.pprint(request.args)
    logging.info("body")
    #pprint.pprint("body")
    pprint.pprint(request.get_json())
    return {"message": "pong"}

if __name__=="__main__":
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S')
    app.run(host='0.0.0.0')
