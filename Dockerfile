FROM python:3.9-slim
COPY src /app
COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

WORKDIR /app
ENV PYTHONUNBUFFERED 1
EXPOSE 5000
#CMD ["python","-u","src/main.py"]
CMD ["gunicorn","--workers","4","--bind","0.0.0.0:5000","wsgi:app"]